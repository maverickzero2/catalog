package id.artivisi.training.telkomsigma.marketplace.catalog.dao;

import id.artivisi.training.telkomsigma.marketplace.catalog.entity.Product;
import id.artivisi.training.telkomsigma.marketplace.catalog.entity.ProductPhotos;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductPhotosDao extends PagingAndSortingRepository<ProductPhotos, String> {
    public Iterable<ProductPhotos> findByProduct(Product product);
}
